extern crate gtk;

use gtk::prelude::*;
use rand::Rng;
use std::cell::RefCell;
use std::cmp::Ordering;
use std::rc::Rc;

struct State {
    entry_val: String,
}

fn main() {
    if gtk::init().is_err() {
        println!("Failed to initialize GTK.");
        return;
    }

    let state = Rc::new(RefCell::new(State {
        entry_val: "".into(),
    }));
    let secret_number = rand::thread_rng().gen_range(1, 101);

    let glade_src = include_str!("guessing_game.ui");
    let builder = gtk::Builder::new_from_string(glade_src);

    let window: gtk::ApplicationWindow = builder.get_object("window1").unwrap();
    let btn_exit: gtk::Button = builder.get_object("btn_exit").unwrap();
    let input_guess: gtk::Entry = builder.get_object("input_guess").unwrap();
    let lbl_response: gtk::Label = builder.get_object("lbl_response").unwrap();
    let btn_submit: gtk::Button = builder.get_object("btn_submit").unwrap();

    btn_exit.connect_clicked(|_| {
        gtk::main_quit();
    });

    window.set_title("Guess the number");
    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    input_guess.set_max_length(2);
    input_guess.connect_changed({
        let state = state.clone();
        move |val| {
            state.borrow_mut().entry_val = val.get_text().unwrap();
        }
    });
    input_guess.connect_activate({
        let lbl_response = lbl_response.clone();
        let state = state.clone();

        move |_| {
            let state = state.borrow();
            let guess: u32 = match state.entry_val.trim().parse() {
                Ok(num) => num,
                Err(_) => return,
            };

            match guess.cmp(&secret_number) {
                Ordering::Less => lbl_response.set_text(&format!("You guessed {}, too small!", guess)),
                Ordering::Greater => lbl_response.set_text(&format!("You guessed {}, too big!", guess)),
                Ordering::Equal => lbl_response.set_text(&format!("You guessed {}, you win!", guess)),
            }
        }
    });

    btn_submit.connect_clicked({
        let lbl_response = lbl_response.clone();
        let state = state.clone();

        move |_| {
            let state = state.borrow();
            let guess: u32 = match state.entry_val.trim().parse() {
                Ok(num) => num,
                Err(_) => return,
            };

            match guess.cmp(&secret_number) {
                Ordering::Less => lbl_response.set_text(&format!("You guessed {}, too small!", guess)),
                Ordering::Greater => lbl_response.set_text(&format!("You guessed {}, too big!", guess)),
                Ordering::Equal => lbl_response.set_text(&format!("You guessed {}, you win!", guess)),
            }
        }
    });

    window.show_all();

    gtk::main();
}
